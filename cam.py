#!/usr/bin/python
import picamera
import time
import json

import os


import argparse
from pythonosc import dispatcher
from pythonosc import osc_server

PATH=os.path.dirname(__file__)

settings= {}

camera = picamera.PiCamera()
camera.resolution = (640, 480)
camera.framerate = 60
camera.start_preview(alpha=150)
camera.brightness =65
camera.contrast = 65


def set_brightness(unused_addr, args, settings_dict):
	print(args)
#	settings_dict['brightness']=float(args)
	update_settings(settings_dict)
	write_settings(settings_dict)
	#camera.brightness=int(args*100)
	print("brightness")
	print(args)

def set_contrast(unused_addr, args, settings_dict):
	camera.contrast=int(args*100)
	print("contrast")
	print(args)

def set_alpha(unused_addr, args, settings_dict):
	camera.preview.alpha=int(args*255)
	print("alpha")
	print(args)

def write_settings(settings_dict):
	print(settings_dict)
	json_item = json.dumps(settings_dict)
	f = open("settings.json","w")
	f.write(json_item)
	f.close()
	print("wrote")

def read_settings(settings):
	with open("%s/settings.json" % PATH) as f_in:
		settings=(json.load(f_in))
	return settings

def update_settings(settings_dict):
	camera.brightness=int(float(settings_dict['brightness'])*100)
	camera.contrast=int(float(settings_dict['contrast'])*100)
	camera.preview.alpha=int(float(settings['alpha'])*255)
	print("update")


settings=read_settings(settings)
update_settings(settings)
write_settings(settings)

# Set ISO to the desired value
# Now fix the values

# Take a picture including the annotation

#camera.image_effect = "negative"



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--ip", default="0.0.0.0", help="The ip to listen on")
	parser.add_argument("--port", type=int, default=5005, help="The port to listen on")
	args = parser.parse_args()

	dispatcher = dispatcher.Dispatcher()
	dispatcher.map("/set/brightness", set_brightness,settings)
	dispatcher.map("/set/contrast", set_contrast,settings)
	dispatcher.map("/set/alpha", set_alpha,settings)

	server = osc_server.ThreadingOSCUDPServer((args.ip, args.port), dispatcher)
	print("Serving on {}".format(server.server_address))


server.serve_forever()

while True:
	time.sleep(1)
	time.sleep(1)
#	camera.image_effect= "none"

while True:
	for effect in camera.IMAGE_EFFECTS:
		camera.image_effect = effect
		camera.annotate_text = "Effect: %s" % effect
		time.sleep(1)
	for awb in camera.AWB_MODES:
		camera.awb_mode = awb
		camera.annotate_text = "AWB: %s" % awb
		time.sleep(1)
	for exposure in camera.EXPOSURE_MODES:
		camera.exposure_mode = exposure
		camera.annotate_text = "EXPOSURE: %s" % exposure
		time.sleep(1)
